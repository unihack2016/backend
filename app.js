const express = require('express')
const path = require('path')
const morgan = require('morgan')
const helmet = require('helmet')
const bodyParser = require('body-parser')

var routes = require('./routes/index')
var api = require('./routes/api')

var mongoose = require('mongoose')

// mongoose.connect('mongodb://localhost:27017/unihack')
mongoose.connect('mongodb://backend:chatshop@ds033015.mlab.com:33015/chatshop')

var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))
db.once('open', function() {
  console.log('Connected to db')
});

var app = express()

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes)
app.use('/api', api)

app.set('views', path.join(__dirname, 'views'))
app.use(morgan('dev'))
app.use(helmet())
app.use(bodyParser.json())

app.use(morgan)

module.exports = app

const port = process.env.PORT || 3000;
app.listen(port)
console.log('Running on ' + port.toString())
