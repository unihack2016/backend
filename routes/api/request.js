var express = require('express')
var router = express.Router()

var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

var Request = require('../../model/request');

router.get('/:status', (req, res) => {
  if (req.query.categories) {
    var categories = req.query.categories.split(',')
  }

  var filter = {}
  if (req.params.status === 'open') {
    filter = {status: 'open'}
  } else if (req.params.status === 'closed') {
    filter = {status: 'closed'}
  }

  Request.find(filter)
    .sort({updatedAt: 1})
    .exec((err, categories) => {
      res.json(categories);
  });
});

router.get('/id/:objectID', (req, res) => {
  Request.findOne({_id: req.params.objectID})
    .exec((err, data) => {
      res.json(data);
  });
});

router.post('/new', jsonParser, (req, res) => {
  var request = new Request({
    title: req.body.title,
    description: req.body.description,
    category: req.body.category,
    username: req.body.username
  })

  request.save((err, req) => {
    if (err) {
      res.status(500).json({ 'Error': 'Unable to save request' })
    }
    else {
      res.status(200).json({ 'Success': 'Saved request' })
    }
  })
});

module.exports = router;
