var express = require('express')
var router = express.Router()

router.use('/request', require('./request'));

router.get('/', function(req, res) {
    res.json({hello: 1})
})

module.exports = router;
