var mongoose = require('mongoose');

var CategorySchema = mongoose.Schema({
  name: String
});

module.exports = mongoose.model('Category', CategorySchema);