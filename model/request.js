var mongoose = require('mongoose');

var RequestSchema = mongoose.Schema({
  title: String,
  user: String,
  description: String,
  category: String,
  photo: String,
  status: String,
  chatSession: String
});

RequestSchema.pre('save', (next) => {
  var request = this;

  var currentDate = new Date();
  request.updatedAt = currentDate;

  if (!this.createdAt) {
    request.createdAt = currentDate;
  }

  next()
})

module.exports = mongoose.model('Request', RequestSchema);