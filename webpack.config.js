const webpack = require('webpack');
const path = require('path');

module.exports = {
  debug: true,
  devtool: 'source-map',
  entry: ['whatwg-fetch', './views/app/app.js'],
  output: {
    path: 'public/build/',
    filename: 'bundle.js',
    sourceMapFilename: '[file].map'
  },
  module: {
    loaders: [
      {test: /\.js$/, exclude: /(node_modules)/, loader: 'babel-loader', query: {presets: ['es2015', 'react']}},
      {test: /\.png$/, loader: 'url-loader?limit=100000'},
      {test: /\.jpg$/, loader: 'file-loader'}
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx'],
    fallback: path.join(__dirname, 'node_modules')
  }
};