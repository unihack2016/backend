var React = require('react')

var RequestChat = React.createClass({
    getInitialState: function() {
        return {
          title: "Chat",
          userName: "Username",
          description: "Descriptive Text"
        };
    },

    componentDidReceiveProps: function(nextProps) {
        this.state.title = nextProps.title
        this.state.userName = nextProps.userName
        this.state.description = nextProps.description
    },

    render: function() {
        return (
            <div id='requestChat' onClick={() => this.props.onClick(this.props.id)} key={this.props.key}>
                <h3>{this.props.category}</h3>
                <subtitle><i className="fa fa-user" aria-hidden="true"></i> {this.props.username}</subtitle>
                <p><i className="fa fa-comment" aria-hidden="true"></i>  {this.props.description}</p>
            </div>
        )
    }
})

module.exports = RequestChat
