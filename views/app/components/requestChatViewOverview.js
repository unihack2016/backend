var React = require('react')

import 'whatwg-fetch';

var RequestChatViewOverView = React.createClass({
    getInitialState: function() {
        return {
            chatKey: "",
            category: "Category",
            userName: "Username",
            description: "Description"
        };
    },

    componentWillReceiveProps: function(nextProps) {
        this.setState({chatKey: nextProps.chatKey})

        var that = this
        fetch('http://localhost:3000/api/request/id/' + nextProps.chatKey).then(function(response) {
            return response.json()
        }).then(function(json) {
            that.setState({title: json.title})
            that.setState({userName: json.username})
            that.setState({description: json.description})
        })
    },

    componentDidReceiveProps: function(nextProps) {
        this.setState({chatKey: nextProps.chatKey})
    },

    render: function() {
        return (
            <div>
                <h3 style={{margin: '20px'}}>Current Chat</h3>
                <div id='requestChat'>
                    <h3>{this.state.category}</h3>
                    <p>{this.state.description}</p>
                </div>
            </div>
        )
    }
})

module.exports = RequestChatViewOverView