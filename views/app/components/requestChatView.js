var React = require('react')
var ChatBubble = require('./chatBubble') 
var {FormGroup, FormControl, Button} = require('react-bootstrap')

var RequestChatView = React.createClass({
    componentDidMount: function() {
        this.sinchClient = new window.SinchClient({
            applicationKey: '784b4f30-c51d-4015-9c6a-245e4e671307',
            capabilities: {messaging: true},
            startActiveConnection: true,
            onLogMessage: function(message) {
                console.log(message);
            }
        });

        var sessionName = 'sinchSession-' + this.sinchClient.applicationKey;
        var sessionObj = JSON.parse(localStorage[sessionName] || '{}');

        var that = this

        this.sinchClient.start({
            username: 'Myer',
            password: 'password'
        }).then(function() {
            // localStorage[sessionName] = JSON.stringify(sinchClient.getSession());
            // console.log('Success!' + localStorage[sessionName]);

            var messageClient = that.sinchClient.getMessageClient();
            // this.setState({messageClient: messageClient})

            var e = {
                onIncomingMessage: function(message) {
                    // console.log('ID: ' + message.messageId)
                    console.log('New message: ' + message.textBody)
                    
                    var updatedChatMessages = that.state.chatMessages.slice();    
                    updatedChatMessages.push(message);   
                    that.setState({ chatMessages: updatedChatMessages })
                },
                onMessageDelivered: function(messageDeliveryInfo) {
                    console.log('Delivered')
                }
            }

            messageClient.addEventListener(e);

            // var sinchMessage = messageClient.newMessage(['Kelly'], 'sent from node')
            // messageClient.send(sinchMessage).fail(handleError)
        })
    },
    getInitialState: function() {
        return {
            messageClient: {},
          title: "Chat",
          userName: "Username",
          description: "Descriptive Text",
          chatMessages: [],
          textBoxText: ""
        };
    },
    handleChange: function(event) {
        this.setState({textBoxText: event.target.value});
    },
    sendMessage: function(event) {
        this.setState({textBoxText: ""});

        var messageClient = this.sinchClient.getMessageClient();

        var sinchMessage = messageClient.newMessage(['Kelly'], this.state.textBoxText)
        messageClient.send(sinchMessage).fail(handleError)
    },
    render: function() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <p>{this.props.description}</p>
                <form>
                    <FormGroup controlId="formBasicText">
                        <FormControl
                            type="text"
                            value={this.state.textBoxText}
                            placeholder="Enter text"
                            onChange={this.handleChange}
                            style={{margin: '10px', width: '500px'}}
                        />
                    </FormGroup>
                </form>
                {/*<input type='text' onChange={this.handleChange} />*/}
                <Button style={{'margin-left': '10px'}} onClick={this.sendMessage}>Send Message</Button>
                <div>
                    {this.state.chatMessages.map(function(message) {
                        return (<ChatBubble message={message} />)
                    }, this)}
                </div>
            </div>
        )
    }
})

module.exports = RequestChatView