var React = require('react')
var ChatList = require('./chatList')
var RequestChat = require('./requestChat')
var RequestChatView = require('./requestChatView')
var RequestChatViewOverview = require('./requestChatViewOverview')
const {Nav, Navbar, NavItem, MenuItem, NavDropdown, Button, Row, Col} = require('react-bootstrap')

var MainView = React.createClass({
    getInitialState: function() {
        return {
            chatKey: "579c6852e71f7a110001fe0c"
        }
    },
    setChat: function(id) {
        console.log('setChat called with ' + id)
        this.setState({chatKey: id})
    },
    render() {
        return (
            <div>
                <Navbar>
                    <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">PocketShop</a>
                    </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                    <NavItem eventKey={1} href="#">Home</NavItem>
                    <NavDropdown eventKey={2} title="Requests" id="basic-nav-dropdown">
                        <MenuItem eventKey={2.1}>Open</MenuItem>
                        <MenuItem eventKey={2.2}>Closed</MenuItem>
                    </NavDropdown>
                    </Nav>
                </Navbar>
                <Col xs={12} md={6}>
                    <ChatList setChat={this.setChat}/>
                </Col>
                <Col xs={6} md={6}>
                    <RequestChatViewOverview chatKey={this.state.chatKey}/>
                    <RequestChatView />
                </Col>
            </div>
        )
    }
});

module.exports = MainView
