var React = require('react')
import { Grid, Row, Col } from 'react-bootstrap';

var MainView = React.createClass({
    render() {
        return (
            <Row>
                {/*<Col xs={3}>
                    <div className='chatUser'>
                        <h3 className="text-center">TEST</h3>
                    </div>
                </Col>*/}
                <Col xs={12}>
                    <div className='chatBubble'>
                        <h3>{this.props.message.senderId}</h3>
                        <p>{this.props.message.textBody}</p>
                        <p className = "timestamp">{(new Date()).getHours() + ":" + ("0" + (new Date()).getMinutes()).slice(-2)}</p>
                    </div>
                </Col>
            </Row>
        )
    }
});

module.exports = MainView
