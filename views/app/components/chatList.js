var React = require('react')
var RequestChat = require('./requestChat')
var SegmentedControl = require('react-segmented-control')

import 'whatwg-fetch';

var ChatList = React.createClass({
    getInitialState: function() {
        return {
            requests: []
        }
    },
    componentDidMount: function() {
        var that = this
        fetch('http://localhost:3000/api/request/all').then(function(response) {
            return response.json()
        }).then(function(json) {
            console.log('parsed json', json)
            that.setState({requests: json})
        })
    },
    handleChatClick: function(key) {
        console.log('setting hat key to '+ key)
        this.props.setChat(key)
    },
    render: function() {
        return (
            <div>
            <h3 style={{margin: '20px'}}>Open Requests</h3>
                {/*<SegmentedControl name="requests" >
                    <span value="open">Open</span>
                    <span value="closed">Closed</span>
                </SegmentedControl>*/}
                {this.state.requests.map(function(request) {
                    return (<RequestChat
                        key={request._id}
                        id={request._id}
                        category={request.category}
                        username={request.username}
                        description={request.description}
                        onClick={this.handleChatClick} />)
                }, this)}
            </div>
        )
    }
})

module.exports = ChatList
