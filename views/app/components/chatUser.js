var React = require('react')

var MainView = React.createClass({
    render() {
        return (
            <div className='chatUser'>
                <h3>{this.props.message.senderId}</h3>
            </div>
        )
    }
});

module.exports = MainView
