var React = require('react')
var ReactDom = require('react-dom')

var MainView = require('./components/mainView')

ReactDom.render(<MainView />, document.getElementById('app'))